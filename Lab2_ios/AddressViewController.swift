//
//  AddressViewController.swift
//  Lab2_ios
//
//  Created by Christofer Berrette on 2017-10-20.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

import MapKit

class AddressViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let latitude: CLLocationDegrees = 59.204992
        
        let longitude: CLLocationDegrees = 17.907203
        
        let latDelta: CLLocationDegrees = 0.02
        
        let longDelta: CLLocationDegrees = 0.02
        
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        
        let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        let region: MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
        
        self.map.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
       
        annotation.title = "Kvällsvägen 12"
        
        annotation.subtitle = "Min familj bor hära"
        
        annotation.coordinate = location
        
        map.addAnnotation(annotation)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
