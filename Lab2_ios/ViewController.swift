//
//  ViewController.swift
//  Lab2_ios
//
//  Created by Christofer Berrette on 2017-10-20.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

import MessageUI

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pressCellphone(_ sender: Any) {
        print("Cellphone")
        let url: URL = URL(string: "TEL://123456789")!
         UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    var pic = ["dog1"]
    
    var sectionlevel = [" ","Work experiance","Demos"]
    let numberOfRowsInSection : [Int] = [5,1,1]
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionlevel.count
    }
    
    @IBAction func burronpressed(_ sender: Any) {
        
        
        let mail = MFMessageComposeViewController()
        mail.messageComposeDelegate = self
        mail.recipients = (["christoffer.berrette@hotmail.com"])
        mail.subject = "heej"
        
        if MFMessageComposeViewController.canSendText()
        {
            self.present(mail, animated: true, completion: nil)
            
        }
        else
        {
            print("buu" )
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionlevel[section]
        
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionlevel
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                
                
                performSegue(withIdentifier: "student", sender: nil)
                
            }
        }
        
        if indexPath.section == 2 {
        if indexPath.row == 0
            {
                performSegue(withIdentifier: "Animate", sender: nil)
            }
        }
        
       
        if indexPath.row == 3
        {
            performSegue(withIdentifier: "Address", sender: nil)
        }
        
        if indexPath.row == 4
        {
            performSegue(withIdentifier: "WebView", sender: nil)
        }
        
      
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if (section == 0) {
                return 0.0
            }
            return UITableViewAutomaticDimension
        }
        
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rows: Int = 0
        
        if section < numberOfRowsInSection.count {
            rows = numberOfRowsInSection[section]
        }
        
        return rows    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ViewControllerTableViewCell
   
        
        
        //cell.button.tag = 1
      
        if indexPath.section == 0
        {
            if indexPath.row-1 == -1
            {
                cell.buttoncelli.isHidden = true
                cell.buttona.isHidden = true
                cell.labelin.text = "Christofer"
                cell.imagenew.image = UIImage(named: pic[indexPath.row] + ".jpg")
                
            }
            if indexPath.row == 1
            {
                //cell.textLabel?.text = "Phone number"
                cell.buttona.isHidden = true
                cell.buttoncelli.isHidden = false
                if cell.buttona.tag == 1 {
                   
                    cell.buttoncelli.setTitle("076 306 82 80", for: .normal)
                    
                }
                
            }
            if indexPath.row == 2
            {
            
                
                    cell.buttoncelli.isHidden = true
                    cell.buttona.isHidden = false
                    cell.buttona.setTitle("Email", for: .normal)
                    
                
            }
            if indexPath.row == 3
            {
                cell.buttoncelli.isHidden = true
                cell.buttona.isHidden = true
                cell.textLabel?.text = "Address"
            }
            if indexPath.row == 4
            {
                cell.buttoncelli.isHidden = true
                cell.buttona.isHidden = true
                cell.textLabel?.text = "Favorite Site"
            }
        }
        
        if indexPath.section == 1
        {
            if indexPath.row == 0 {
            cell.buttoncelli.isHidden = true
            cell.buttona.isHidden = true
            cell.textLabel?.text = "JU Education"
            }
        }
        
        if indexPath.section == 2 {
        
             if indexPath.row == 0 {
            cell.buttoncelli.isHidden = true
            cell.buttona.isHidden = true
                cell.textLabel?.text = "Click Here to se some Animation"
                
            }
        }
        
        
      
      
        
        return cell
        
    }

   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 0 && indexPath.row == 0 {
        return 120
    }else {
        return 50
    }
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

